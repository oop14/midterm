/*
 * To change Aritsuthis license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.midterm;

/**
 *
 * @author 
 */
public class Television {
    int size;
    String screen;
    String color;
    int volume = 25;
    int channel = 1;
    int status = 0;
    public Television(int size, String color,String screen ){
        this.size = size;
        this.screen = screen;
        this.color = color;
    }
    public void turnOn(){
         status = 1;
         System.out.println("Television is turn on now. ");
    }
    public void turnOff(){
         status = 0;
         System.out.println("Television is turn off now."); 
    }
    public void volumeTV(char symbol){
        if (status == 1){
            if(symbol == '+'){
                volume++;
            }if(symbol =='-'){
                volume--;
            }
        }else{
            System.out.println("Please turn on the television ");
        }
        
    }
    
    
    public void changeCh(char symbol){
        if (status == 1){
            if(symbol == '+'){
                channel++;
            }if(symbol =='-'){
                channel--;
            }
        }else{
            System.out.println("Please turn on the television ");
        }
        
    }
    public void changeCh(int channel){
        if (status == 1){
            this.channel= channel;
        }else{
            System.out.println("Please turn on the television ");
        }
        
    }
    
    
    @Override
    public String toString(){
        return this.screen+" TV "+this.size+" inch color: "+ this.color+" Watching Channel: "+this.channel+" volume: "+this.volume;  
        
    }
    
    
}
